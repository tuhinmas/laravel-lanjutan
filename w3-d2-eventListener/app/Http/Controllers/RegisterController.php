<?php

namespace App\Http\Controllers;

use Mail;
use App\User;
use Illuminate\Http\Request;
use App\Mail\UserRegisteredMail;
use App\Events\UserRegisteredEvent;
use App\Http\Requests\RegisterRequest;

class RegisterController extends Controller
{
    public function daftar(RegisterRequest $request){
        $user = new User;
        $user->name = $request->name;
        // dd($user->name);
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        // dd($user);
        $user->save();

        event(new UserRegisteredEvent($user));
        
        return response('register berhasil', 200);
    }
}
