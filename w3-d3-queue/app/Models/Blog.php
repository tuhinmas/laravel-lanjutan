<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = ['user_id','title','isi','publish_status'];
    public function user(){
        return $this->belongsTo('App\User');
    }
}
