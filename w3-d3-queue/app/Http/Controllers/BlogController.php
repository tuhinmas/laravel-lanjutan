<?php

namespace App\Http\Controllers;

use Mail;
use App\Models\Blog;
use App\Mail\NewBlogMail;
use App\Events\NewBlogEvent;
use Illuminate\Http\Request;
use App\Http\Requests\BlogRequest;

class BlogController extends Controller
{
    public function newBlog(BlogRequest $request){

        $user = auth()->user();
        $blog = new Blog;
        $blog->user_id = $user->id;
        $blog->title = request('title');
        $blog->isi = request('isi');

        $blog->save();

        event(new NewBlogEvent($blog));
        return response("sukses");
    }
}
