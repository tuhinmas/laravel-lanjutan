<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(LoginRequest $request)
    {
        // dd($user);

        if(!$token = auth()->attempt($request->only('email','password'))){
            return response(null, 200);
        }
        $user = auth()->user();
        // dd($user);
        return response()->json(compact(['token','user']));
    }
}
