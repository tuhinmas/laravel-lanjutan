<?php

namespace App\Listeners;

use Mail;
use App\Mail\NewBlogMail;
use App\Events\NewBlogEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNewBlogEmailNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->blog;
    }

    /**
     * Handle the event.
     *
     * @param  NewBlogEvent  $event
     * @return void
     */
    public function handle(NewBlogEvent $event)
    {
        // dd($event);
        Mail::to(auth()->user())->send(new NewBlogMail($event->blog));
    }

    public function failed(OrderShipped $event, $exception)
    {
        //
    }
}
