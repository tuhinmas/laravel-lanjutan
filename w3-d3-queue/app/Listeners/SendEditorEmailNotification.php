<?php

namespace App\Listeners;

use Mail;
use App\Mail\NewBlogMail;
use App\Events\NewBlogEvent;
use App\Mail\SendEditorMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEditorEmailNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewBlogEvent  $event
     * @return void
     */
    public function handle(NewBlogEvent $event)
    {
        Mail::to("editor@gmail.com")->send(new SendEditorMail($event->blog));
    }
}
