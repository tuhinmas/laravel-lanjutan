<?php

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('paragraf','Redis\RedisController@index');
Route::get('paragraf-tanpa-cache','Redis\RedisController@tanpaCache');
Route::get('/store','Redis\RedisController@store');



Route::get('cv',function(){
    // Cache::put('username','tuhinmas',60);
    // dd(Cache::get('username'));

    // Redis::set('nama','mastuhin',2);
    // dd(Redis::get('nama'));

    $data = Cache::store('redis')->put('bar', 'baz', 60);
    dd($data);

});
