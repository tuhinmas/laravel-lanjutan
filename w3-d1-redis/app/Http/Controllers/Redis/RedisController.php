<?php

namespace App\Http\Controllers\Redis;

use App\Models\Red;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

class RedisController extends Controller
{
    public function index(){
        $data = Cache::remember('paragraf', 20, function(){
            return Red::all();
        });
        // $data = Redis::set('paragraf',Red::all());
        return view('redis', compact('data'));
    }

    public function store(){
        $paragraf = "cache adalah data yang disimpan sementara di memori. cache terbagi menjadi dua, yaitu client side cache dan server side cache. Redis adalah salah satu bentuk contoh server side cache. Redis dipilih karena kecepatannya dalam melakukan caching.";
        $data = explode(" ", $paragraf);
        // dd($data);
        foreach ($data as $item){
            $newItem = Redis::create(['item' => $item]);
        }
    }
    
    public function tanpaCache(){
        $data = Red::all();
        return view('redis', compact('data'));
    }
    
}
