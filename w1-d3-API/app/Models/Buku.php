<?php

namespace App\Buku;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = 'buku';
    protected $guarded = [];
    public $timestamps = false;

    public function peminjaman(){
        $this->hasOne('App\Models\Peminjaman');
    }
}
