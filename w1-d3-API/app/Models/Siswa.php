<?php

namespace App;

use App\Models\Peminjaman;
use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'siswa';
    protected $guarded = [];
    public  $timestamps = false;
    public function user(){
        return $this->hasOne('App\Models\User','siswa','user_id');
    }

    public function peminjaman(){
        $this->hasMany(Peminjaman::class);
    }
}
