<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Peminjaman extends Model
{
    protected $table = 'peminjaman';
    protected $guarded = [];
    public $timestamps = false;

    public function buku(){
        return $this->hasOne('App\Models\Buku','peminjaman','buku_id');
    }

    public function user(){
        return $this->belongsTo('App\Models\User','peminjaman','user_id');
    }
}
