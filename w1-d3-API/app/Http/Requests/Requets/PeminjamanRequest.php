<?php

namespace App\Http\Requests\Requets;

use Illuminate\Foundation\Http\FormRequest;

class PeminjamanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'buku_id' => 'required',
            'tanggalPinjam' => ['required','date'],
            'batasAkhirPeminjaman' => ['required','date'],
            'kembali' => 'required',
            'onTime' => 'required'
        ];
    }
}
