<?php

namespace App\Http\Requests\Requets;

use Illuminate\Foundation\Http\FormRequest;

class MahasiswaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => ['numeric','required'],
            'nama' => 'required',
            'nim' => ['required', 'unique:siswa,nim'],
            'fakultas' => 'required',
            'jurusan' => 'required',
            'hp' => ['numeric', 'required'],
            'wa' => ['numeric', 'required']
        ];
    }
}
