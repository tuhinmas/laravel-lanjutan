<?php

namespace App\Http\Controllers;

use App\Buku\Buku;
use Illuminate\Http\Request;
use App\Http\Requests\Request\BukuRequest;
use App\Http\Resources\Resources\BukuResource;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku = Buku::get();
        return BukuResource::collection($buku);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return 'create';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BukuRequest $request)
    {
        $buku = Buku::create($this->insertBuku());
        return $buku;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $buku = Buku::find($id);
        return $buku;
        // return BukuResource::collection($buku);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return 'edit';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        Buku::where('kodeBuku',$id)
              ->update($this->insertBuku());
        $buku = Buku::where('kodeBuku',$id)->get();
        return $buku;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Buku::where('kodeBuku',$id)
              ->delete();
        return response('Buku Berhasil dihapus');
    }
    public function insertBuku(){
        return [
            'kodeBuku' => request('kodeBuku'),
            'judul' => request('judul'),
            'pengarang' => request('pengarang'),
            'tahunTerbit' => request('tahunTerbit')
        ];
    }

}
