<?php

namespace App\Http\Controllers;

use App\Models\Peminjaman;
use Illuminate\Http\Request;
use App\Http\Requests\Requets\PeminjamanRequest;
use App\Http\Resources\Resources\PeminjamanResource;

class PeminjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($user_id)
    {
        $peminjaman = Peminjaman::where('user_id',$user_id)
                                  ->get();
        $peminjamanCollection = PeminjamanResource::collection($peminjaman);
        return $peminjamanCollection;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PeminjamanRequest $request)
    {
        $peminjaman = Peminjaman::create([
            'user_id' => request('user_id'),
            'buku_id' => request('buku_id'),
            'tanggalPinjam' => request('tanggalPinjam'),
            'batasAkhirPeminjaman' => request('batasAkhirPeminjaman'),
            'kembali' => request('kembali'),
            'onTime' => request('onTime')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id,$id)
    {
        $peminjaman = Peminjaman::find($id);
        $peminjamanCollection = PeminjamanResource::collection($peminjaman);
        return $peminjamanCollection;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PeminjamanRequest $request, $id)
    {
        $peminjaman = Peminjamana::where('id',$id)
                                   ->update([
                                        'user_id' => request('user_id'),
                                        'buku_id' => request('buku_id'),
                                        'tanggalPinjam' => request('tanggalPinjam'),
                                        'batasAkhirPeminjaman' => request('batasAkhirPeminjaman'),
                                        'kembali' => request('kembali'),
                                        'onTime' => request('onTime')
                                   ]);
        $data = Peminjaman::find($id);
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Peminjaman::delete()->where('id',$id);
        return response('Data Berhasil diHapus');
    }
    public function kembali($id){
        $peminjaman = Peminjaman::find($id);
        $onTime = false;

        if(!date("Y-m-d") >= $peminjaman->batasAkhirPeminjaman){
            $onTime = true;
            Peminjaman::where('id',$id)
                        ->update([
                            'kembali' => date("Y-m-d"),
                            'onTime' => '1'
                        ]);
            }
            return Peminjaman::find($id);
        
    }
}
