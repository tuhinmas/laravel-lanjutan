<?php

namespace App\Http\Controllers;

use App\Siswa;
use Illuminate\Http\Request;
use App\Http\Requests\Requets\MahasiswaRequest;
use App\Http\Resources\Resources\MahasiswaResource;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return 'index mahasiswa';
        $data = Siswa::all();
        $dataCollection = MahasiswaResource::collection($data);
        return $dataCollection;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MahasiswaRequest $request)
    {
        $mahasiwa = Siswa::create([
            'user_id' => request('user_id'),
            'nama' => request('nama'),
            'nim' => request('nim'),
            'fakultas' => request('fakultas'),
            'jurusan' => request('jurusan'),
            'hp' => request('hp'),
            'wa' => request('wa')

        ]);
        return $mahasiwa;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($nim)
    {
        $mahasiswa = Siswa::where('nim',$nim)
                            ->get();
        $mahasiswaCollection = MahasiswaResource::collection($mahasiswa);
        return $mahasiswaCollection;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MahasiswaRequest $request, $nim)
    {
        $update = Siswa::update([
                            'user_id' => request('user_id'),
                            'nama' => request('nama'),
                            'nim' => request('nim'),
                            'fakultas' => request('fakultas'),
                            'jurusan' => request('jurusan'),
                            'hp' => request('hp'),
                            'wa' => request('wa')
                        ])
                        ->where('nim',$nim);
        $mahasiswa = Siswa::where('nim',$nim)
                            ->get();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($nim)
    {
        Siswa::delete()->where("nim",$nim);
        return response('Data Mahasiswa Berhasil diHapus');
    }
}
