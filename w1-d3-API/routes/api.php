<?php


    Route::post('register','Auth\RegisterController');
    Route::post('login','Auth\LoginController'); 
    Route::post('logout','Auth\LogoutController');
    Route::get('user', 'UserController');

Route::middleware('auth:api')->group(function(){
    Route::post('cek','BukuController@index');
    Route::get('buku', 'BukuController@index');
    Route::get('buku/{kode_buku}', 'BukuController@show');

    Route::get('mahasiswa', 'MahasiswaController@index');

});

Route::middleware('auth:api','role:2')->group(function(){
    Route::post('buku/store', 'BukuController@store');
    Route::patch('buku/update/{kode_buku}', 'BukuController@update');
    Route::delete('buku/delete/{kode_buku}', 'BukuController@destroy');
    
    Route::get('mahasiswa/{nim}', 'MahasiswaController@show');
    Route::post('mahasiswa/store', 'MahasiswaController@store');
    Route::patch('mahasiswa/update/{nim}', 'MahasiswaController@update');
    Route::delete('mahasiswa/delete/{nim}', 'MahasiswaController@destroy');

    Route::get('peminjaman/{user_id}', 'PeminjamanController@index');
    Route::get('peminjaman/{id}', 'PeminjamanController@show');
    Route::post('peminjaman/create', 'PeminjamanController@store');
    Route::put('peminjaman/update/{user_id}/{id}', 'PeminjamanController@update')->name('peminjaman.update');
    Route::delete('peminjaman/delete/{id}', 'PeminjamanController@destroy');    
});


