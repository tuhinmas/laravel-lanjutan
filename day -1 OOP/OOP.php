<?php
trait Hewan{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;
    public function atraksi(){
        return $this->nama." sedang ".$this->keahlian;
    }
}

trait Fight{
    public $attackPower;
    public $defencePower;
    public function serang($korban){
        return $this->nama." sedang menyerang ".$korban->nama;
    }

    public function diserang($penyerang){
        $this->darah = $this->darah - ($penyerang->attackPower/$this->defencePower);
        return $this->nama." sedang diserang";
    }
}

class Elang{
    use Hewan;
    use Fight;
    public function __construct(){
        $this->jumlahKaki = 2;
        $this->nama = "Elang_3";
        $this->darah;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }
    public function getInfoHewan(){
       echo "<b>Data".$this->nama."</b><br>";
       echo "Nama :".$this->nama."<br>";
       echo "Jumlah kaki: ".$this->jumlahKaki."<br>"; 
       echo "Keahlian: ".$this->keahlian."<br>"; 
       echo "Attack Power: ".$this->attackPower."<br>"; 
       echo "Defence Power: ".$this->defencePower."<br>"; 
    }
}

class Harimau{
    use Hewan;
    use Fight;
    public function __construct(){
        $this->jumlahKaki = 4;
        $this->nama = "Harimau_1";
        $this->darah;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }
    public function getInfoHewan(){
       echo "<b>Data ".$this->nama."</b><br>";
       echo "Nama :".$this->nama."<br>";
       echo "Jumlah kaki: ".$this->jumlahKaki."<br>"; 
       echo "Keahlian: ".$this->keahlian."<br>"; 
       echo "Attack Power: ".$this->attackPower."<br>"; 
       echo "Defence Power: ".$this->defencePower."<br>"; 
    }
}

$elang = new Elang();
$elang->getInfoHewan();
echo "<br>";
$harimau = new Harimau();
$harimau->getInfoHewan();

echo "<br>";
echo $elang->atraksi()."<br>";
echo $harimau->atraksi()."<br>";

echo "<br>";
echo "<b>Action</b>";
echo "<br>";
echo $elang->serang($harimau);
echo "<br>";
echo $harimau->serang($elang);
echo "<br><br>";

echo "<b>Tarung</b><br>";
echo "Round 1: ".$harimau->diserang($elang).". Sisa darah ".$harimau->darah."<br>";
echo "Round 2: ".$harimau->diserang($elang).". Sisa darah ".$harimau->darah."<br>";
echo "Round 3: ".$elang->diserang($harimau).". Sisa darah ".$elang->darah."<br>";
echo "Round 4: ".$elang->diserang($harimau).". Sisa darah ".$elang->darah."<br>";