<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'role:3'], function(){
    Route::get('/route-3','HomeController@route')->name('route-3');    
});

Route::group(['middleware' => 'role:2'], function(){
    Route::get('/route-2','HomeController@route')->name('route-2');
});

Route::group(['middleware' => 'role:1'], function(){
    Route::get('/route-1','HomeController@route')->name('route-1');
});


