<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $user = Auth::user()->role_id;
        if($user == $role){
            return $next($request);
        }
        elseif ($user == 2 && $role == 3){
            return $next($request);
        }
        else if($user == 1 && ($role == 3 || $role == 2)){
            return $next($request);
        }
        abort(403);
    }
}
